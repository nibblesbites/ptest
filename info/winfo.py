#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# vim: set ts=2 sw=2 sts=2 et:


import lxml.html
import requests
import signal
import sys

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


if sys.argv.__len__() < 2:
  print("Use: %s <url>")
  sys.exit(1)


TARGET_URL = sys.argv[1].rstrip('/')
USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"


def key_ctrlc_handler(signal, frame):
  print()
  sys.exit(0)
signal.signal(signal.SIGINT, key_ctrlc_handler)


requests.models.CONTENT_CHUNK_SIZE = 100 * 1024
def get_url( url ):
  headers = {
    'user-agent': USER_AGENT
  }
  r = requests.get( url, headers=headers, verify=False )
  return r


def get_links( html ):
  l_list = []
  doc = lxml.html.document_fromstring( r.text )
  links = doc.xpath('//@href')

  for l in links:
    if l not in l_list:
      l_list.append(l)
  return l_list


def get_root_domain( link ):
  return '.'.join( link.split('//')[1].split('/')[0].split('.')[-2:] )


def filter_local_link( link ):
  if link.lower().startswith('https://') or link.lower().startswith('http://') or link.lower().startswith('//'):
    if TARGET_ROOT_DOMAIN != get_root_domain( link ):
      return False
    if link.lower().startswith('tel:') or link.lower().startswith('mailto:') or link.lower().startswith('#') or link.lower().startswith('javascript:'):
      return False
  return True

def full_link( link ):
  if l.lower().startswith('https://') or l.lower().startswith('http://'):
    return l
  elif l.lower().startswith('//'):
    return TARGET_URL + l[1:] 
  elif l.lower().startswith('/'):
    return TARGET_URL + l
  else:
    return TARGET_URL +'/'+ l



TARGET_ROOT_DOMAIN = get_root_domain( TARGET_URL )
r = get_url( TARGET_URL )

links = {
  TARGET_URL: True
}

user_input_links = {}

for l in get_links( r.text ):
  if l not in links and filter_local_link( l ):
    if '?' in l:
      user_input_links[l] = False
    else:
      links[l] = False


print('--------- headers ----------')
for h in r.headers:
  print('%s: %s' % (h, r.headers[h]))


print()
print()
print('--------- user input links ----------')
for l in user_input_links.keys():
  print( full_link(l) )


print()
print()
print('--------- other links ----------')
for l in links.keys():
  print( full_link(l) )
